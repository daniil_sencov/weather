//
//  MainViewController.swift
//  Weather
//
//  Created by Daniil on 28.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import UIKit
import CoreLocation

struct JournalData {
    var weather: WeatherData
    var location: CLLocationCoordinate2D
}

struct WeatherData {
    var locationName: String?
    var locationRegion: String?
    var temperature: Float?
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    private var notification: NSObjectProtocol?
    var locationWorker: LocationWorker?
    var weatherWorker: WeatherWorker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let lastLocation = UserDefaults.standard.value(forKey: "last_location") as? String {
            self.locationLabel.text = lastLocation
        }
        if let lastTemperature = UserDefaults.standard.value(forKey: "last_temperature") as? Float {
            self.temperatureLabel.text = "\(String(describing: (lastTemperature))) °C"
        }
        
        getWeather()
        
        notification = NotificationCenter.default.addObserver(forName: .UIApplicationWillEnterForeground, object: nil, queue: .main) {
            [unowned self] notification in
            self.getWeather()
        }
    }
    
    func showError(_ error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { action in
            self.getWeather()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getWeather() {
        if locationWorker == nil {
            locationWorker = LocationWorker(completion: self.location)
        }
    }
    
    func location( _ location: CLLocation?)
    {
        self.locationWorker = nil
        if location == nil {
            showAcessDeniedAlert()
            return
        }
        if weatherWorker == nil {
            weatherWorker = WeatherWorker()
        }
        weatherWorker?.getJson(latitude: (location?.coordinate.latitude)!, longtitude: (location?.coordinate.longitude)!) { (weather: WeatherData?, error:NSError?, errorMsg:String?) in
            DispatchQueue.main.async(execute: {
                if error != nil {
                    let errorString = (error?.domain)!
                    self.showError(errorString)
                } else if errorMsg != nil {
                    self.showError(errorMsg!)
                } else {
                    self.locationLabel.text = "\((weather?.locationName)!)/\((weather?.locationRegion)!)"
                    self.temperatureLabel.text = "\(String(describing: (weather?.temperature!)!)) °C"
                    
                    UserDefaults.standard.set(self.locationLabel.text , forKey: "last_location")
                    UserDefaults.standard.set(weather?.temperature!, forKey: "last_temperature")
                    
                    let journalData: JournalData = JournalData(weather: weather!, location: (location?.coordinate)!)
                    let dataWorker: DataWorker = DataWorker()
                    
                    dataWorker.save(journalData: journalData)
                }
                self.weatherWorker = nil
            })
        }
    }
    
    func showAcessDeniedAlert() {
        let alertController = UIAlertController(title: "Location Accees Requested",
                                                message: "The location permission was not authorized. Please enable it in Settings to continue.",
                                                preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
            
            // THIS IS WHERE THE MAGIC HAPPENS!!!!
            if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(appSettings as URL)
            }
        }
        alertController.addAction(settingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        if let notification = notification {
            NotificationCenter.default.removeObserver(notification)
        }
    }
}

