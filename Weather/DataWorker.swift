//
//  DataWorker.swift
//  Weather
//
//  Created by Daniil on 29.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import UIKit
import CoreData

class DataWorker: NSObject {
    override init() {
        print("[DataWorker]: init")
    }
    func save(journalData: JournalData) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Weather",
                                                in: managedContext)!
        
        let weather = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        weather.setValue(journalData.weather.locationName, forKeyPath: "name")
        weather.setValue(journalData.weather.locationRegion, forKeyPath: "region")
        weather.setValue(journalData.weather.locationRegion, forKeyPath: "region")
        weather.setValue(journalData.weather.temperature, forKeyPath: "temperature")
        weather.setValue(journalData.location.latitude, forKeyPath: "lat")
        weather.setValue(journalData.location.longitude, forKeyPath: "lon")
        weather.setValue(Date(), forKeyPath: "date")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    deinit {
        print("[DataWorker]: deinit")
    }
}
