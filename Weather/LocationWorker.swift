//
//  LocationWorker.swift
//  Weather
//
//  Created by Daniil on 29.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import Foundation
import CoreLocation

class LocationWorker: NSObject, CLLocationManagerDelegate {
    
    let locationManager: CLLocationManager
    
    typealias CompletionHandler = ( _ location: CLLocation?) -> Void
    let completion: CompletionHandler
    
    init(completion: @escaping CompletionHandler) {
        print("[LocationWorker]: init")
        self.completion = completion
        
        self.locationManager = CLLocationManager()
        super.init()
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .denied {
            self.completion(self.locationManager.location)
        }
    }
    
    deinit {
        print("[LocationWorker]: deinit")
    }
}
