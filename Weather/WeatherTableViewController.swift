//
//  WeatherTableViewController.swift
//  Weather
//
//  Created by Daniil on 30.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import UIKit
import CoreData

class CustomCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var coordsLabel: UILabel!
}

class WeatherTableViewController: UITableViewController {

    var weatherList: [NSManagedObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "The Weather List"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Weather")
        do {
            weatherList = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let weather = weatherList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell",
                                                 for: indexPath) as! CustomCell
        
        //region
        let name = weather.value(forKeyPath: "name") as? String
        let region = weather.value(forKeyPath: "region") as? String
        if name != nil && region != nil {
            cell.nameLabel?.text = "\(name!)/\(region!)"
        } else {
            cell.nameLabel?.text = "Unknown"
            
        }
        
        //remperature
        if let temp = weather.value(forKeyPath: "temperature") as? Float {
            cell.temperatureLabel?.text = "\(String(describing: temp)) °C"
        } else {
            cell.temperatureLabel?.text = "Unknown"
        }
        
        //coords
        let lat = weather.value(forKeyPath: "lat") as? Double
        let lon = weather.value(forKeyPath: "lon") as? Double
        if lat != nil && lon != nil {
            cell.coordsLabel?.text = "\(String(describing: round(100*lat!)/100))/\(String(describing: round(100*lon!)/100))"
        } else {
            cell.coordsLabel?.text = "Unknown"
        }
        
        //date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        if let date = weather.value(forKeyPath: "date") as? Date {
            cell.timeLabel.text = dateFormatter.string(from: date)
        } else {
            cell.timeLabel.text = "Unknown"
        }
        return cell
    }
}
