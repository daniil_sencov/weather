//
//  WeatherWorker.swift
//  Weather
//
//  Created by Daniil on 28.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import UIKit

class WeatherWorker: NSObject {
    
    let endUrl: String = "http://api.apixu.com/v1"
    let apiKey: String = "1c20d5e1e4dc4e729b2143554172808"


    override init() {
        print("[WeatherWorker]: init")
    }
    func getJson(latitude: Double, longtitude: Double, _ onCompletion: @escaping (WeatherData?, NSError?, String?) -> Void)
    {
        let urlPath: String = endUrl + "/current.json?key=" + apiKey + "&q=" + String(latitude) + "," + String(longtitude)
        let url: URL = URL(string: urlPath)!
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            if (error != nil) {
                onCompletion(nil, error as NSError?, nil)
                return
            }
            let httpResponse = response as? HTTPURLResponse
            let code = httpResponse?.statusCode
            if (code == 200) {
                let weather = self.parseJson(data!)
                if weather.locationName != nil && weather.locationRegion != nil && weather.temperature != nil {
                    print("[WeatherWorker]: succes get weather data")
                    onCompletion(weather, nil, nil)
                } else {
                    onCompletion(nil, nil, "Error parse json")
                }
            } else {
                onCompletion(nil, nil, "Response error without error message")
            }
            
        })
        task.resume()
    }
    func parseJson(_ data: Data) -> WeatherData {
        var weather = WeatherData()
        let json: AnyObject! = self.nsdataToJSON(data) as AnyObject!
        if let responseObject = json as? [String: AnyObject] {
            
            //get location
            if let location = responseObject["location"] {
                if let locationData = location as? [String: AnyObject] {
                    weather.locationName = locationData["name"] as? String
                    weather.locationRegion = locationData["region"] as? String
                }
            }
            //get current
            if let current = responseObject["current"] {
                if let currentData = current as? [String: AnyObject] {
                    weather.temperature = Float((currentData["temp_c"] as? Float)!)
                }
            }
        }
        return weather
    }
    func nsdataToJSON(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    deinit {
        print("[WeatherWorker]: deinit")
    }
}
