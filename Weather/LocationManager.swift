//
//  LocationManager.swift
//  Weather
//
//  Created by Daniil on 31.08.17.
//  Copyright © 2017 Daniil. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class LocationManager: NSObject, CLLocationManagerDelegate {

    private var manager: CLLocationManager?
    
    private struct SharedInstance {
        static var instance: LocationManager?
    }
    
    class func getInstance() -> LocationManager
    {
        if (SharedInstance.instance == nil)  {
            SharedInstance.instance = LocationManager()
        }
        return SharedInstance.instance!
    }
    
    override init() {
        super.init()
        requestAuth()
    }
    
    func startMonitoringSignificantLocationChanges()
    {
        self.manager = CLLocationManager()
        self.manager?.delegate = self
        self.manager!.startMonitoringSignificantLocationChanges()
    }
    
    func stopMonitoringSignificantLocationChanges()
    {
        self.manager!.stopMonitoringSignificantLocationChanges()
        self.manager = nil
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        let location: CLLocation? = locations.last as? CLLocation
        if (location == nil) { return }

        let weatherWorker = WeatherWorker()
        
        weatherWorker.getJson(latitude: (location?.coordinate.latitude)!, longtitude: (location?.coordinate.longitude)!) { (weather: WeatherData?, error:NSError?, errorMsg:String?) in
            DispatchQueue.main.async(execute: {
                if error == nil && errorMsg == nil {
                    
                    if let lastTemperature = UserDefaults.standard.value(forKey: "last_temperature") as? Float {
                        if (weather?.temperature!)! - lastTemperature > 3.0 || (weather?.temperature!)! - lastTemperature < -3.0 {
                            self.showNotification (temperature: (weather?.temperature!)!)
                        }
                    }
                    self.showNotification (temperature: (weather?.temperature!)!) //TODO REMOVE
                    
                    let journalData: JournalData = JournalData(weather: weather!, location: (location?.coordinate)!)
                    let dataWorker: DataWorker = DataWorker()
                    
                    dataWorker.save(journalData: journalData)
                }
            })
        }
        
    }
    
    func requestAuth() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.alert, .sound]) { (granted, error) in
                print("callback")
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func showNotification(temperature: Float)
    {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            content.title = "Temperature has changed"
            content.body = "Now temperature is \(String(describing:temperature)) °C"
            content.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            center.add(request)
        } else {
            let notification = UILocalNotification()
            notification.alertBody = "Temperature has changed"
            notification.alertAction = "Now temperature is \(String(describing:temperature)) °C"
            notification.soundName = UILocalNotificationDefaultSoundName
            
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }
}
